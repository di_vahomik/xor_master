﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace XOR
{
    public partial class Form1 : Form
    {
        //  public CreatingKey key_create;
        string filetext;
     //текст
        string Shifr_Key; //ключ

        string Shifr_text;
        
        string DeShifr_text;
        

        public Form1()
        {
            InitializeComponent();
            

            openFileDialog1.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
            saveFileDialog1.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";

            SAVE_SHIFR_TEXT.Enabled = false;
            SAVE_DECOD_TEXT.Enabled = false;
            BUTTON_SHIFR_TEXT.Enabled = false;
        }

        //генерация случайного ключа
        private void GEN_KEY_Click(object sender, EventArgs e)
        {
            string key_generate = "abcdefghijklmnopqrstuvwxyz0123456789";
            int key_length = 8;
            Shifr_Key = Generate_Random_string(key_generate, key_length);
            KEY_BOX.Text = Shifr_Key;
        }
        private string Generate_Random_string(string in_string, int lengt)//генерация ключа из строки
        {
            Random rnd = new Random();
            StringBuilder gen_random_key = new StringBuilder(lengt);
            int key_pos = 0;
            for (int i = 0; i < lengt; i++)
            {
                key_pos = rnd.Next(0, in_string.Length - 1);
                gen_random_key.Append(in_string[key_pos]);
            }
            return gen_random_key.ToString();
        }

        //Если текст не кратен ключу дополняем его нулями
        static string Text_DEL_KEY(string in_string, string key)
        {
            
            while (in_string.Length % key.Length != 0)
            {
                in_string += "\0";
            }
            return in_string;

        }
        
        //Реализация метода XOR
        string XOR(string in_string, string key)
        {

            Encoding type;
            type = Encoding.Unicode;
            var textInBytes = type.GetBytes(in_string); // перевод в  двоичную ситсему текста
            var keyInBytes = type.GetBytes(key);// перевод в  двоичную ситсему ключа
            byte[] res = new byte[textInBytes.Length];
            for (var i = 0; i < textInBytes.Length; i++)
            {
                res[i] = Convert.ToByte(textInBytes[i] ^ keyInBytes[i % keyInBytes.Length]);
            }
            return type.GetString(res);
        }

        //Вывод текста с файл на экран
        private void BUTTON_TEXT_FROM_COMP_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;
            filetext = System.IO.File.ReadAllText(filename);
            TEXT_FROM_WINDOW.Text = filetext;
        }

        //Сохранение декодированного текста
        private void SAVE_DECOD_TEXT_Click(object sender, EventArgs e)
        {

            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = saveFileDialog1.FileName;
            System.IO.File.WriteAllText(filename, TEXT_FROM_WINDOW.Text);
        }

        //сохранение шифрованного текста
        private void SAVE_SHIFR_TEXT_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = saveFileDialog1.FileName;
            System.IO.File.WriteAllText(filename, SHIFR_TEXT_FROM_WINDOW.Text);
        }

        //Кнопка шифровки/расшифровки текста
        private void BUTTON_SHIFR_TEXT_Click(object sender, EventArgs e)
        {
            // filetext = TEXT_FROM_WINDOW.Text;

            filetext = Text_DEL_KEY(TEXT_FROM_WINDOW.Text, Shifr_Key);
            if (RADIO_BUTTON_SHIFR.Checked)
            {
    
                Shifr_text = XOR(filetext, Shifr_Key);
                SHIFR_TEXT_FROM_WINDOW.Text = Shifr_text;

                SAVE_SHIFR_TEXT.Enabled = true;
            }

            if (RADIO_BUTTON_DECODER.Checked)
            {
                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
                string filename = openFileDialog1.FileName;
                string codedtext = System.IO.File.ReadAllText(filename);
                SHIFR_TEXT_FROM_WINDOW.Text = codedtext;

                DeShifr_text = XOR(Shifr_text, Shifr_Key);
                TEXT_FROM_WINDOW.Text = DeShifr_text;
                SAVE_DECOD_TEXT.Enabled = true;
            }

        }

        private void RADIO_BUTTON_SHIFR_CheckedChanged(object sender, EventArgs e)
        {
            if (RADIO_BUTTON_SHIFR.Checked == true)
            {
                BUTTON_SHIFR_TEXT.Enabled = true;
            }
        }

        private void RADIO_BUTTON_DECODER_CheckedChanged(object sender, EventArgs e)
        {
            if (RADIO_BUTTON_DECODER.Checked == true)
            {
                SAVE_SHIFR_TEXT.Enabled = false;
            }
        }
    }
}
